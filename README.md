Piezo-Sensor
------------

A standalone piezo conditioning board for use as an impact sensor.

The circuit takes the noisy input voltage signal from the attached piezo,
rectifies it, clips it to 5V and passes through an RC pair to give
a pulse-and-discharge signal.

The pulse duration is approximately 10ms long, and the maximum
voltage relates to the impact size on the piezo, so it's only
triggered by an impact and not a slow press.

By choosing a threshold voltage level on the attached device, it's
possible to filter out impacts above a required size. The RC circuit
is specifically sized to be useful for registering strikes from
foam dart guns such as those from Nerf guns.
