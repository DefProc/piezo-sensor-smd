EESchema Schematic File Version 2
LIBS:sensor-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:sensor-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "2 feb 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ZENER D3
U 1 1 54CFDFC5
P 4500 2050
F 0 "D3" H 4500 2150 50  0000 C CNN
F 1 "ZENER DIODE" H 4500 1950 40  0000 C CNN
F 2 "Diodes_SMD:D_MiniMELF" H 4500 2050 60  0000 C CNN
F 3 "~" H 4500 2050 60  0000 C CNN
F 4 "Zener Diode, Single, 3.3V, 500mW, SOD-80(MiniMELF)" H 50  -1500 50  0001 C CNN "Description"
F 5 "BZT55C3V3-GS08" H 4500 2050 60  0001 C CNN "MPN"
F 6 "7104216" H 4500 2050 60  0001 C CNN "rs"
F 7 "2690030" H 4500 2050 60  0001 C CNN "farnell"
F 8 "BZT55C3V3-GS08-ND" H 50  -1500 50  0001 C CNN "digi-key"
	1    4500 2050
	0    -1   1    0   
$EndComp
$Comp
L DIODE D2
U 1 1 54CFDFE1
P 4150 2050
F 0 "D2" H 4150 2150 40  0000 C CNN
F 1 "SHOTTKY DIODE" H 4200 1950 40  0000 C CNN
F 2 "Diodes_SMD:D_SMB" H 4150 2050 60  0000 C CNN
F 3 "~" H 4150 2050 60  0000 C CNN
F 4 "Shottky Rectifier diode, 1A, 100V, SMB" H 50  -1500 50  0001 C CNN "Description"
F 5 "1861422" H 50  -1500 50  0001 C CNN "farnell"
F 6 "SS110B" H 50  -1500 50  0001 C CNN "MPN"
	1    4150 2050
	0    1    -1   0   
$EndComp
$Comp
L DIODE D1
U 1 1 54CFE004
P 3650 1600
F 0 "D1" H 3650 1500 40  0000 C CNN
F 1 "SHOTTKY DIODE" H 3650 1700 40  0000 C CNN
F 2 "Diodes_SMD:D_SMB" H 3650 1600 60  0000 C CNN
F 3 "~" H 3650 1600 60  0000 C CNN
F 4 "Shottky Rectifier diode, 1A, 100V, SMB" H 50  -1500 50  0001 C CNN "Description"
F 5 "1861422" H 50  -1500 50  0001 C CNN "farnell"
F 6 "SS110B" H 50  -1500 50  0001 C CNN "MPN"
	1    3650 1600
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 54CFE013
P 4850 2050
F 0 "R1" V 4930 2050 40  0000 C CNN
F 1 "1M 1/4W" V 4750 2050 40  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 4780 2050 30  0000 C CNN
F 3 "~" H 4850 2050 30  0000 C CNN
F 4 "1M 0603 (any manufacturer)" H 50  -1500 50  0001 C CNN "Description"
F 5 "LR0204F1M0" H 50  -1500 50  0001 C CNN "MPN"
F 6 "477-8662" H 50  -1500 50  0001 C CNN "rs"
F 7 "2447285" H 50  -1500 50  0001 C CNN "farnell"
F 8 "CF14JT1M00CT-ND" H 50  -1500 50  0001 C CNN "digi-key"
	1    4850 2050
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 54CFE022
P 5200 2050
F 0 "C1" H 5200 2150 40  0000 L CNN
F 1 "4.7nF" H 5206 1965 40  0000 L CNN
F 2 "Resistors_SMD:R_0603" H 5238 1900 30  0000 C CNN
F 3 "~" H 5200 2050 60  0000 C CNN
F 4 "4.7nF Capacitor 0603 (any manufacturer)" H 50  -1500 50  0001 C CNN "Description"
F 5 "MCFU5472Z5" H 50  -1500 50  0001 C CNN "MPN"
F 6 "721-5287" H 50  -1500 50  0001 C CNN "rs"
F 7 "1759098" H 50  -1500 50  0001 C CNN "farnell"
F 8 "BC1076CT-ND" H 50  -1500 50  0001 C CNN "digi-key"
	1    5200 2050
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-sensor #PWR01
U 1 1 54CFE05E
P 5550 2500
F 0 "#PWR01" H 5550 2500 30  0001 C CNN
F 1 "GND" H 5550 2430 30  0001 C CNN
F 2 "" H 5550 2500 60  0000 C CNN
F 3 "" H 5550 2500 60  0000 C CNN
	1    5550 2500
	1    0    0    -1  
$EndComp
$Comp
L PIEZO PZ1
U 1 1 54D000ED
P 3150 2000
F 0 "PZ1" H 3000 1750 60  0000 C CNN
F 1 "PIEZO" H 2950 2250 60  0000 C CNN
F 2 "project_fp:leaded-piezo_nostrain" H 3150 2000 60  0001 C CNN
F 3 "" H 3150 2000 60  0000 C CNN
F 4 "20mm uncased piezo (https://www.kitronik.co.uk/c3309-20mm-uncased-piezo.html)" H 50  -1500 50  0001 C CNN "Description"
F 5 "490-7712-ND" H 50  -1500 50  0001 C CNN "digi-key"
F 6 "1192520" H 3150 2000 60  0001 C CNN "farnell"
F 7 "7BB-20-6L0 " H 3150 2000 60  0001 C CNN "PartNumber"
	1    3150 2000
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 P1
U 1 1 54D00142
P 5950 2400
F 0 "P1" H 5900 2200 40  0000 C CNN
F 1 "PULSE" V 6050 2350 40  0000 C CNN
F 2 "project_fp:conn_1x2_no-strain" H 5950 2400 60  0001 C CNN
F 3 "" H 5950 2400 60  0000 C CNN
F 4 "2-way header footprint, no part (DNP)" H 5950 2400 60  0001 C CNN "Description"
	1    5950 2400
	1    0    0    1   
$EndComp
Text GLabel 5750 1600 2    60   Output ~ 0
PULSE
Text GLabel 2700 3500 0    60   Input ~ 0
PULSE
$Comp
L BSS138 Q1
U 1 1 5A688684
P 3050 3500
F 0 "Q1" H 3250 3575 50  0000 L CNN
F 1 "BSS138" H 3250 3500 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:TSOT-23" H 3250 3425 50  0001 L CIN
F 3 "" H 3050 3500 50  0001 L CNN
F 4 "N-channel FET, SOT-23" H 3050 3500 60  0001 C CNN "Description"
F 5 "BSS138" H 3050 3500 60  0001 C CNN "MPN"
F 6 "6710324" H 3050 3500 60  0001 C CNN "rs"
F 7 "9845330" H 3050 3500 60  0001 C CNN "farnell"
F 8 "BSS138CT-ND" H 3050 3500 60  0001 C CNN "digi-key"
	1    3050 3500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 5A688775
P 3150 3900
F 0 "#PWR02" H 3150 3650 50  0001 C CNN
F 1 "GND" H 3150 3750 50  0000 C CNN
F 2 "" H 3150 3900 50  0001 C CNN
F 3 "" H 3150 3900 50  0001 C CNN
	1    3150 3900
	1    0    0    -1  
$EndComp
$Comp
L VDD #PWR03
U 1 1 5A688799
P 3150 2900
F 0 "#PWR03" H 3150 2750 50  0001 C CNN
F 1 "VDD" H 3150 3050 50  0000 C CNN
F 2 "" H 3150 2900 50  0001 C CNN
F 3 "" H 3150 2900 50  0001 C CNN
	1    3150 2900
	1    0    0    -1  
$EndComp
Text GLabel 3400 3250 2    60   Output ~ 0
~TRIGGER
$Comp
L Conn_01x03 P2
U 1 1 5A6889B8
P 5700 3500
F 0 "P2" H 5700 3700 50  0000 C CNN
F 1 "Trigger" H 5700 3300 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x03_Pitch2.54mm" H 5700 3500 50  0001 C CNN
F 3 "" H 5700 3500 50  0001 C CNN
F 4 "3-way header footprint, no-part (DNP)" H 5700 3500 60  0001 C CNN "Description"
	1    5700 3500
	1    0    0    1   
$EndComp
Text GLabel 5200 3500 0    60   Input ~ 0
~TRIGGER
$Comp
L VDD #PWR04
U 1 1 5A688E60
P 5300 3300
F 0 "#PWR04" H 5300 3150 50  0001 C CNN
F 1 "VDD" H 5300 3450 50  0000 C CNN
F 2 "" H 5300 3300 50  0001 C CNN
F 3 "" H 5300 3300 50  0001 C CNN
	1    5300 3300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 5A688E86
P 5300 3700
F 0 "#PWR05" H 5300 3450 50  0001 C CNN
F 1 "GND" H 5300 3550 50  0000 C CNN
F 2 "" H 5300 3700 50  0001 C CNN
F 3 "" H 5300 3700 50  0001 C CNN
	1    5300 3700
	1    0    0    -1  
$EndComp
$Comp
L R_Small R2
U 1 1 5A689AC5
P 3150 3050
F 0 "R2" H 3180 3070 50  0000 L CNN
F 1 "10K" H 3180 3010 50  0000 L CNN
F 2 "Resistors_SMD:R_0603" H 3150 3050 50  0001 C CNN
F 3 "" H 3150 3050 50  0001 C CNN
F 4 "10k Resistor, 75V, 0603 (any manufacturer)" H 3150 3050 60  0001 C CNN "Description"
F 5 "MCMR06X103 JTL" H 3150 3050 60  0001 C CNN "MPN"
F 6 "2073356" H 3150 3050 60  0001 C CNN "farnell"
	1    3150 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 1600 5750 1600
Wire Wire Line
	3150 2400 5750 2400
Wire Wire Line
	5550 2400 5550 2500
Wire Wire Line
	4150 1850 4150 1600
Wire Wire Line
	4500 1850 4500 1600
Wire Wire Line
	4850 1900 4850 1600
Wire Wire Line
	5200 1900 5200 1600
Wire Wire Line
	4150 2250 4150 2400
Wire Wire Line
	4500 2400 4500 2250
Wire Wire Line
	4850 2400 4850 2200
Wire Wire Line
	5200 2400 5200 2200
Wire Wire Line
	3150 1650 3150 1600
Wire Wire Line
	3150 1600 3450 1600
Wire Wire Line
	3150 2350 3150 2400
Wire Wire Line
	5550 1600 5550 2300
Wire Wire Line
	5550 2300 5750 2300
Connection ~ 4150 1600
Connection ~ 4500 1600
Connection ~ 4850 1600
Connection ~ 5200 1600
Connection ~ 4150 2400
Connection ~ 4500 2400
Connection ~ 4850 2400
Connection ~ 5200 2400
Connection ~ 5550 2400
Connection ~ 5550 1600
Wire Wire Line
	3150 3700 3150 3900
Wire Wire Line
	2700 3500 2850 3500
Wire Wire Line
	5500 3400 5300 3400
Wire Wire Line
	5300 3400 5300 3300
Wire Wire Line
	5500 3500 5200 3500
Wire Wire Line
	5300 3700 5300 3600
Wire Wire Line
	5300 3600 5500 3600
Wire Wire Line
	3150 2900 3150 2950
Wire Wire Line
	3150 3150 3150 3300
Wire Wire Line
	3400 3250 3150 3250
Connection ~ 3150 3250
$EndSCHEMATC
